﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LongJump : MonoBehaviour {

    [SerializeField]
    GameObject marker; //Jump location marker
    bool isMarkerEnabled = false; 

    void Update () {

        if (Input.GetKey(KeyCode.LeftControl))
        {
            //Ray from center of camera
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            
            if (Physics.Raycast(ray, out hit, 100))
            {
                //Detect whether the object hit is jump able
                if (hit.transform.CompareTag("Jumpable"))
                {
                    if (!isMarkerEnabled)
                        marker.SetActive(true);

                    //Set marker position if surface detected is jump able
                    marker.transform.position = new Vector3(hit.point.x, hit.point.y + 0.5f, hit.point.z);
                }
                else
                {
                    marker.SetActive(false);
                }
            }
            else
            {
               marker.SetActive(false);
            }
        }
        else
            marker.SetActive(false);

        //Teleport player to location of marker when control is released
        if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            transform.position = marker.transform.position;
        }
    }
}
